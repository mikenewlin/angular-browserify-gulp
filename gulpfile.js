var gulp = require('gulp');
var less = require('gulp-less');
var connect = require('gulp-connect');
var watch = require('gulp-watch');
// requires browserify and vinyl-source-stream
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var Server = require('karma').Server;
var sequence = require('run-sequence');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var minifyCSS = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');
var minifyHTML = require('gulp-minify-html');
var rev = require('gulp-rev');
var revReplace = require("gulp-rev-replace");
var clean = require("gulp-clean");
var argv = require('yargs').argv;
var ngHtml2Js = require("gulp-ng-html2js");
var concat = require("gulp-concat");
var gzip = require('gulp-gzip');

gulp.task('webserver', function () {
    connect.server({
        root: ['node_modules', 'tmp'],
        port: process.env.PORT,
        livereload: {
            port: 8081
        }
    });
});

gulp.task('clean-scripts', function () {
  return gulp.src('./tmp/js/', {read: false})
    .pipe(clean());
});

gulp.task('clean-css', function () {
  return gulp.src('./tmp/css/', {read: false})
    .pipe(clean());
});

gulp.task('minify-html', function() {
  var opts = {
    conditionals: true,
    spare:true
  };

  var manifestJS = gulp.src('./tmp/js/rev-manifest.json');
  var manifestCSS = gulp.src('./tmp/css/rev-manifest.json');
  return gulp.src('./app/*.html')
    .pipe(revReplace({manifest: manifestJS}))
    .pipe(revReplace({manifest: manifestCSS}))    
    .pipe(minifyHTML(opts))
    .pipe(gulp.dest('./tmp/'));
});

gulp.task('views', function() {
  return gulp.src("./app/views/*.html")
    .pipe(minifyHTML({
        empty: true,
        spare: true,
        quotes: true
    }))
    .pipe(ngHtml2Js({
        moduleName: "app.views"
    }))
    .pipe(concat("views.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./tmp/js"));
  
});

gulp.task('scripts', ['clean-scripts'], function() {
    // Grabs the app.js file
    
  var bundler = browserify('./app/scripts/app.js');
  return bundler
    // bundles it and creates a file called main.js
    .bundle()
    .pipe(source('main.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(rev())
    .pipe(sourcemaps.write())
    // saves it the public/js/ directory
    .pipe(gulp.dest('./tmp/js'))
    .pipe(rev.manifest())
    .pipe(gulp.dest('./tmp/js'))
    .on('error', gutil.log);

});

gulp.task('styles', ['clean-css'], function () {
  return gulp.src('./app/styles/*.less')
    .pipe(sourcemaps.init())
    .pipe(less())
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))    
    .pipe(minifyCSS())
    .pipe(rev())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./tmp/css'))      
    .pipe(rev.manifest())
    .pipe(gulp.dest('./tmp/css'))    
    .on('error', gutil.log);
});

gulp.task('livereload', function() {
  gulp.src(['tmp/css/*.css', 'tmp/js/*.js'])
    .pipe(watch('tmp/css/*.css'))
    .pipe(watch('tmp/js/*.js'))
    .pipe(watch('tmp/*.html'))
    .pipe(connect.reload())
    .on('error', gutil.log);
});

gulp.task('watch', function() {
    gulp.watch('app/scripts/**/*.js', ['scripts']);
    gulp.watch('app/styles/style.less', ['styles']) ; 
    gulp.watch('app/index.html', ['minify-html']);
    gulp.watch('app/views/**/*.html', ['views']);
});

/**
 * Run test once and exit
 */
gulp.task('karma', function (done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, done).start();
});

gulp.task('build', function(callback) {
  sequence('scripts', 'styles', 'views', 'minify-html', callback);
});

gulp.task('test', function(callback) {
  sequence('build', 'karma', callback);
});

gulp.task('run', function(callback) { 
  gutil.log(argv.env);
  sequence('build', 'webserver', 'livereload', 'watch', callback);
});  