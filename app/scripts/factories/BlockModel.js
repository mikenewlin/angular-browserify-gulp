/*global angular*/
module.exports = (function() {
    'use strict';

    angular
        .module('serviceAccounts')
        .factory('BlockModel', BlockModelFactory);
      
    BlockModelFactory.$inject = ['$log', '$q', '_'];    
    
    function BlockModelFactory($log, $q, _) {
        

        function BlockModel(id, desc) {
            
            this.id = id;
            this.desc = desc;
            this.open = false;
        }
        
        BlockModel.prototype = {
            setData: function(data) {
                if (data && data.id && data.desc) {
                    angular.extend(this, data);
                }    
            },
            getAlerts: function() {
                if (_.isUndefined(this.alerts)) {
                    this.clearAlerts();
                }                    
                return this.alerts;
            },
            removeAlert: function(index) {
                if (_.isUndefined(this.alerts)) {
                    this.clearAlerts();
                }
                else {
                    this.alerts.splice(index, 1);
                }
            },
            clearAlerts: function() {
                this.alerts = [];
            },
            addAlert: function(alert) {
                if (_.isUndefined(this.alerts)) {
                    this.clearAlerts();
                }                    
                this.alerts.push(alert);
            },      
        };
        
        BlockModel.validate = function(data) {
            return (data && data.id && data.desc);
        }
        
        BlockModel.build = function(data) {
          return new BlockModel(data.id, data.desc);  
        };
        
        return BlockModel;
    }    
})();    