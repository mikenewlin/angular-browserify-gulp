/*global angular*/
module.exports = (function() {
    'use strict';

    angular
        .module('serviceAccounts')
        .factory('SubserviceModel', SubserviceModelFactory);
      
    SubserviceModelFactory.$inject = ['$log', '$q', '_'];    
    
    function SubserviceModelFactory($log, $q, _) {
       

        function SubserviceModel(id, desc, selected) {
            this.id = id;
            this.desc = desc;
            this.selected = selected;
        }
        
        SubserviceModel.prototype = {
            setData: function(data) {
                if (data && data.id && data.desc && data.selected) {
                    angular.extend(this, data);
                }    
            },
        };
        
        SubserviceModel.validate = function(data) {
            return (data && data.id && data.desc && data.selected);
        }
        
        SubserviceModel.build = function(data) {
            var model = new SubserviceModel(data.id, data.desc, data.selected);
            return model;
        };       
        
        return SubserviceModel;
    }    
})();    