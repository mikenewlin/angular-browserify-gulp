/*global angular Block Service*/
module.exports = (function() {
    'use strict';

    angular
        .module('serviceAccounts')
        .factory('DataServices', DataServices);
      
    DataServices.$inject = ['$log', '$q', '_', 'BlockModel', 'ServiceModel', 'AccountModel', 'SubserviceModel', 'BlockService', 'ServiceService', 'AccountService', 'SubserviceService', '$cacheFactory'];    
    
    function DataServices($log, $q, _, BlockModel, ServiceModel, AccountModel, SubserviceModel, BlockService, ServiceService, AccountService, SubserviceService, $cacheFactory) {
        $log.debug("DataServices: constructed: ");
        var serviceAccountCache = $cacheFactory("serviceAccountCache");

        var factory = {
            getBlockList: getBlockList,
            getBlock: getBlock,
            getServiceList: getServiceList,
            getService: getService,
            loadCopyToServices: loadCopyToServices,
            loadAccounts: loadAccounts,
            loadSubservices: loadSubservices,
            save: save
        };
    
        return factory;
        
    	function getBlockList() {
    		$log.debug("DataServices: getBlockList: " + serviceAccountCache.info());
			var deferred = $q.defer();
    		var cache = serviceAccountCache.get("blocks");
    		if (cache) {
				deferred.resolve(cache);
    		}
    		else {
    			BlockService.getList().then(
    				function (result) {
	            	   	var blocks = _.map(result, BlockModel.build);
	                	serviceAccountCache.put("blocks", blocks);
	                   	deferred.resolve(blocks);
    				},
    				function (error) {
    					deferred.reject(error);
    				}
    			);
    		}
			return deferred.promise;    		
    	}

    	function getBlock(id) {
    		var cache = serviceAccountCache.get("blocks");
    		if (cache) {
    			return _.find(cache, 'id', id);
    		}
    		else {
    			return null;
    		}
    	}
    	
    	function getServiceList(blockId) {
    		$log.debug("DataServices: getServiceList: blockId: " + blockId);
    		var deferred = $q.defer();
    		var cache = serviceAccountCache.get(blockId);
    		if (cache) {
    			$log.debug("DataServices: getServiceList: from cache: ");
				deferred.resolve(cache);

    		}
    		else {
    			ServiceService.getList(blockId).then(
    				function (result) {
	                	var services = _.map(result, ServiceModel.build);
	                	serviceAccountCache.put(blockId, services);
	                   	deferred.resolve(services);			
    				},
    				function (error) {
    					deferred.reject(error);
    				}
    			);	
    		}
    		return deferred.promise;
    	}

 		function getService(blockId, serviceId) {
    		var cache = serviceAccountCache.get(blockId);
    		if (cache) {
    			return _.find(cache, 'id', serviceId);
    		}
    		else {
    			return null;
    		}
 		}
 		
    	function loadCopyToServices(blockId, service) {
    		$log.debug("DataServices: loadCopyToServices: blockId: " + blockId + " serviceId: " + service.id);
    		var deferred = $q.defer();
    		if (!service.hasCopyToServices()) {
	    		var cache = serviceAccountCache.get(blockId);
	    		if (cache) {
	    			$log.debug("DataServices: loadCopyToServices: from cache: ");
					service.setCopyToServices(cache);
					deferred.resolve(service);
		    	}
		    	else {
	    			ServiceService.getList(blockId).then(
	    				function (result) {
		                	var services = _.map(result, ServiceModel.build);
							service.setCopyToServices(services);
		                   	deferred.resolve(service);			
		    			},
		    			function(error) {
		    				service.addAlert({type: 'danger', msg: error.statusText});
		    				deferred.reject(error);
		    			}
		    		);	
		    	}
    		}
    		else {
    			deferred.resolve(service);
    		}
	    	return deferred.promise;
    	} 
    	
    	function loadAccounts(blockId, service) {
    		$log.debug("DataServices: loadAccounts: blockId: " + blockId + " serviceId: " + service.id);
    		var deferred = $q.defer();
    		if (!service.hasAccounts()) {
	    		var cache = serviceAccountCache.get("accounts:" + blockId + service.id);
	    		if (cache) {
	    			$log.debug("DataServices: loadAccounts: from cache: ");
	    			service.setAccounts(cache)
					deferred.resolve(service);
		    	}
		    	else {
		    		AccountService.getList(blockId, service.id).then(
		    			function(result) {
		    				service.setAccounts(_.map(result, AccountModel.build));
							serviceAccountCache.put("accounts:" + blockId + service.id, result);
							deferred.resolve(service);		    				
		    			},
		    			function(error) {
		    				service.addAlert({type: 'info', msg: error.statusText});
		    				service.allAccounts = true;
		    				deferred.reject(error);
		    			}
		    		);	
		    	}
    		}
    		else {
    			deferred.resolve(service);
    		}
	    	return deferred.promise;
    	} 
    	
    	function loadSubservices(blockId, service) {
    		if (service.selectedAccount === 'All') {
    			return loadServiceSubservices(blockId, service);
    		}
    		else {
    			return loadAccountSubservices(blockId, service);
    		}
    		
    	}
    	
    	function loadServiceSubservices(blockId, service) {
    		$log.debug("DataServices: loadServiceSubservices: blockId: " + blockId + " serviceId: " + service.id);
    		var deferred = $q.defer();
    		if (!service.hasSubservices()) {
	    		var cache = serviceAccountCache.get("subservices:" + blockId + service.id);
	    		if (cache) {
	    			$log.debug("DataServices: loadServiceSubservices: from cache: ");
	    			service.setSubservices(cache);
					deferred.resolve(service);
		    	}
		    	else {
		    		SubserviceService.getList(blockId, service.id).then(
		    			function(result) {
		    				var subservices = _.map(result, SubserviceModel.build);
		    				service.setSubservices(subservices);
							serviceAccountCache.put("subservices:" + blockId + service.id, subservices);
							deferred.resolve(service);		    				
		    			},
		    			function(error) {
		    				service.addAlert({type: 'info', msg: error.statusText});
		    				service.allSubServices = true;
		    				service.showSubservices = false;
		    				deferred.reject(error);
		    			}
		    		);
		    	}
    		}
    		else {
    			deferred.resolve(service);
    		}
	    	return deferred.promise;
    	}  		
    	
    	function loadAccountSubservices(blockId, service) {
    		var account = service.getSelectedAccount();
    		$log.debug("DataServices: loadAccountSubservices: blockId: " + blockId + " serviceId: " + service.id + " account: " + account.account);
    		var deferred = $q.defer();
    		if (!account.hasSubservices()) {
	    		var cache = serviceAccountCache.get("accountSubservices:" + blockId + service.id + account.account);
	    		if (cache) {
	    			$log.debug("DataServices: loadAccountSubservices: from cache: ");
	    			account.setSubservices(cache);
					deferred.resolve(account);
		    	}
		    	else {
		    		SubserviceService.getAccountList(blockId, service.id, account.account).then(
		    			function(result) {
		    				var subservices = _.map(result, SubserviceModel.build);
		    				account.setSubservices(subservices);
							serviceAccountCache.put("accountSubservices:" + blockId + service.id + account.account, subservices);
							deferred.resolve(account);		    				
		    			},
		    			function(error) {
		    				service.addAlert({type: 'danger', msg: error.statusText});
		    				service.allSubServices = true;
		    				deferred.reject(error);
		    			}
		    		);		    			
		    	}
    		}
    		else {
    			deferred.resolve(account);
    		}
	    	return deferred.promise;
    	}  	
    	
    	function save(service) {
    		service.clearAlerts();
    		var deferred = $q.defer();
			ServiceService.save(ServiceModel.savedata(service)).then(
				function (result) {
                   	deferred.resolve(service);			
    			},
    			function(error) {
    				service.addAlert({type: 'danger', msg: error.statusText});
    				deferred.reject(error);
    			}
    		);    		
    		return deferred.promise;
    	}
    }    
})();