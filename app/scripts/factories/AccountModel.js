/*global angular*/
module.exports = (function() {
    'use strict';

    angular
        .module('serviceAccounts')
        .factory('AccountModel', AccountModelFactory);
      
    AccountModelFactory.$inject = ['$log', '$q', '_', '$rootScope'];    
    
    function AccountModelFactory($log, $q, _, $rootScope) {
       

        function AccountModel(account, selected) {
            this.account = account;
            this.selected = selected;
			this.selectedSubservicesCount = 0;
        }
        
        AccountModel.prototype = {
            setData: function(data) {
                if (data && data.account && data.selected) {
                    angular.extend(this, data);
                }    
            },
            showSubserviceList: function(service) {
                $log.debug("AccountModel: showSubserviceList: " + this.account);
                service.selectedAccount = this.account;
                if (service.showSubservices) {
                    //need to reload grid if already showing
                    $rootScope.$broadcast('subserviceListController::activate', service);
                }
                else {
                    service.showSubservices = true;
                }  
            },  
            hasWarning: function(allSubservices) {
                //! grid.appScope.service.allSubServices && row.entity.selected && row.entity.selectedSubservicesCount == 0    
                if (allSubservices) {
                    return false;
                }
                else {
                    return (this.selected && this.selectedSubservicesCount == 0);
                }
            },
            clearSubservices: function() {
                this.subservices = [];
            },
            hasSubservices: function() {
                if (_.isUndefined(this.subservices)) {
                    return false;
                }    
                else {
                    return !_.isEmpty(this.getSubservices());  
                }    
            },
            setSubservices: function(data) {
                if (_.isUndefined(this.subservices)) {
                    this.clearSubservices();
                }                  
                this.subservices = data;
            },
            getSubservices: function() {
                if (_.isUndefined(this.subservices)) {
                    this.clearSubservices();
                }                 
                return this.subservices;
            },
            getSubservice: function(id) {
                return _.find(this.getSubservices(), 'id', id);
            }             
        };
        
        AccountModel.validate = function(data) {
            return (data && data.account && data.selected);
        }
        
        AccountModel.build = function(data) {
            var model = new AccountModel(data.account, data.selected);
            return model;
        };       
        
        return AccountModel;
    }    
})();    