/*global angular*/
module.exports = (function() {
    'use strict';

    angular
        .module('serviceAccounts')
        .factory('ServiceModel', ServiceModelFactory);
      
    ServiceModelFactory.$inject = ['$log', '$q', '_', '$rootScope', 'AccountModel'];    
    
    function ServiceModelFactory($log, $q, _, $rootScope, AccountModel) {
       

        function ServiceModel(id, desc, allAccounts, allSubServices) {
            this.id = id;
            this.desc = desc;
            this.allAccounts = allAccounts;
            this.allSubServices = allSubServices;
            this.open = false;
			this.selectedAccount = "All";
			this.showSubservices = false;
			this.selectedAccountCount = 0;
			this.selectedSubservicesCount = 0;
			this.changeAllSwitch();			
        }
        
        ServiceModel.prototype = {
            setData: function(data) {
                if (data && data.id && data.desc, data.hasAccounts && data.allSubServices) {
                    angular.extend(this, data);
                }    
            },
            getAlerts: function() {
                if (_.isUndefined(this.alerts)) {
                    this.clearAlerts();
                }                    
                return this.alerts;
            },
            removeAlert: function(index) {
                if (_.isUndefined(this.alerts)) {
                    this.clearAlerts();
                }
                else {
                    this.alerts.splice(index, 1);
                }
            },
            clearAlerts: function() {
                this.alerts = [];
            },
            addAlert: function(alert) {
                if (_.isUndefined(this.alerts)) {
                    this.clearAlerts();
                }                    
                this.alerts.push(alert);
            },         
            changeAllSwitch: function() {
                if (this.allAccounts && !this.allSubServices) {
                    this.selectedAccount = "All";
                    //need to reload the grid if already showing
                    if (this.showSubservices) {
                        $rootScope.$broadcast('subserviceListController::activate', this);
                    }
                    else {
                        this.showSubservices = true;
                    }
                }
                else {
                    this.showSubservices = false;
                }
            }, 
            selectedCopyToServiceDesc: function() {
                var selected = this.selectedCopyToService();
                if (_.isUndefined(selected)) {
                    return "";
                }
                else {
                    return selected.desc;
                }
            },            
            selectedCopyToService: function() {
                return _.find(this.getCopyToServices(), 'selected', true);    
            },
            copyTo: function(service) {
                var copyToService = _.find(this.copyToServices, 'selected', true);
                copyToService.selected = false;
                service.selected = true;
            },
            hasCopyToServices: function() {
                if (_.isUndefined(this.copyToServices)) {
                    return false;
                }    
                else {
                    return !_.isEmpty(this.getCopyToServices());  
                }    
            },            
            addCopyToService: function(copyTo) {
                if (_.isUndefined(this.copyToServices)) {
                    this.clearCopyToServices();
                }    
                if (_.isEmpty(this.copyToServices)) {
                    this.copyToServices.push({id: '98', desc: 'All', selected: false});
    			    this.copyToServices.push({id: '99', desc: 'None', selected: true});
                }
                if (copyTo.id != this.id) {
                    this.copyToServices.push(copyTo);  
                }    
            },
            clearCopyToServices: function() {
                this.copyToServices = [];
            },
            setCopyToServices: function(services) {
                this.clearCopyToServices();
                var self = this;
                _.forEach(services, function(service) {
                    self.addCopyToService({id: service.id, desc: service.desc, selected: false});    
                });  
                this.copyToServices = _.sortBy(this.copyToServices, 'id');
            },
            getCopyToServices: function() {
                if (_.isUndefined(this.copyToServices)) {
                    this.clearCopyToServices();
                }
                return this.copyToServices;
            },
            hasAccountWarning: function() {
                return (! this.allAccounts && this.selectedAccountCount == 0);    
            },
            clearAccounts: function() {
                this.accounts = [];
            },
            hasAccounts: function() {
                if (_.isUndefined(this.accounts)) {
                    return false;
                }
                else {
                    return !_.isEmpty(this.getAccounts());
                }    
            },
            setAccounts: function(data) {
                if (_.isUndefined(this.accounts)) {
                    this.clearAccounts();
                }                
                this.accounts = _.map(data, AccountModel.build);
            },
            getAccounts: function() {
                if (_.isUndefined(this.accounts)) {
                    this.clearAccounts();
                }                
                return this.accounts;
            },
            getAccount: function(account) {
                return _.find(this.getAccounts(), 'account', account);
            },
            getSelectedAccount: function() {
                if (this.selectedAccount != 'All') {
                    return _.find(this.getAccounts(), 'account', this.selectedAccount);
                }
                else {
                    return null;
                }
            },       
            hasSubserviceWarning: function() {
                return (! this.allSubServices && this.allAccounts && this.selectedSubservicesCount == 0);    
            }, 
            clearSubservices: function() {
                this.subservices = [];
            },
            hasSubservices: function() {
                if (_.isUndefined(this.subservices)) {
                    return false;
                }    
                else {
                    return !_.isEmpty(this.getSubservices());  
                }    
            },
            setSubservices: function(data) {
                if (_.isUndefined(this.subservices)) {
                    this.clearSubservices();
                }                  
                this.subservices = data;
            },
            getSubservices: function() {
                if (_.isUndefined(this.subservices)) {
                    this.clearSubservices();
                }                 
                return this.subservices;
            },
            getSubservice: function(id) {
                return _.find(this.getSubservices(), 'id', id);
            }            
            
        };
        
        ServiceModel.savedata = function(service) {
            var saveModel = {};
            saveModel.id = service.id;
            saveModel.allAccounts = service.allAccounts;
            saveModel.allSubServices = service.allSubServices;
            saveModel.copyToService = service.selectedCopyToService().desc;
            saveModel.changes = [];
            if (service.allAccounts && service.allSubServices) {
                var self = service;
                if (service.hasAccounts()) {
                    _.forEach(service.getAccounts(), function(account) {
                        if (self.hasSubservices()) {
                            _.forEach(self.getSubservices(), function(subservice) {
                                saveModel.changes.push({account: account.account, subservice: subservice.id});
                            });                           
                        }
                        else {
                            saveModel.changes.push({account: account.account, subservice: ""});
                        }
                    });
                }
                else {
                    if (service.hasSubservices()) {
                        saveModel.changes = _.map(service.getSubservices(), function(subservice) { return {account: "", subservice: subservice.id}});
                    }
                }
            }
            else if (service.allSubServices) {
                var accountSelected = _.where(service.getAccounts(), {selected: true});
                saveModel.changes = _.map(accountSelected, function(account) { return {account: account.account, subservice: ""}});
            }
            else if (service.allAccounts) {
                var subserviceSelected = _.where(service.getSubservices(), {selected: true});
                saveModel.changes = _.map(subserviceSelected, function(subservice) { return {account: "", subservice: subservice.id}});                 
            }
            else {
                accountSelected = _.where(service.getAccounts(), {selected: true});
                _.forEach(accountSelected, function(account) {
                    var subserviceSelected = _.where(account.getSubservices(), {selected: true});
                    _.forEach(subserviceSelected, function(subservice) { 
                        saveModel.changes.push({account: account.account, subservice: subservice.id});
                    });
                });   
            }
            return saveModel;
        }
        
        ServiceModel.validate = function(data) {
            return (data && data.id && data.desc, data.hasAccounts && data.allSubServices);
        }
        
        ServiceModel.build = function(data) {
            var model = new ServiceModel(data.id, data.desc, data.allAccounts, data.allSubServices);
            return model;
        };       
        
        return ServiceModel;
    }    
})();    