/*global angular*/
module.exports = (function() {
    'use strict';

    angular
        .module('serviceAccounts')
        .controller('BlockController', BlockController);
        
    BlockController.$inject = ['$log', 'DataServices', '_'];    
    
    function BlockController($log, DataServices, _) {
        var vm = this;
        
        $log.debug("BlockController: loading: ");
        vm.blocks = [];
        vm.serviceTemplate = 'services.html';
        activate();
    
        ////////////
    
        function activate() {
            $log.debug("BlockController: activate: ");
    	    DataServices.getBlockList()
                .then(function (result) {
                    vm.blocks = result;
                    $log.debug("BlockController: getBlockList: load blocks ")
                }, function (error) {
                    $log.debug("BlockController: getBlockList error: " + error.statusText);
                }
            );    
            $log.debug("BlockController: activate finished: ");
        }          
    }    
        
})();