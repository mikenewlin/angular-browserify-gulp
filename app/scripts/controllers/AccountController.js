/*global angular*/
module.exports = (function() {
  'use strict';

  angular
    .module('serviceAccounts')
    .controller('AccountController', AccountController);

  AccountController.$inject = ['$log', 'DataServices'];

  function AccountController($log, DataServices) {
      var vm = this;
      $log.debug("AccountController: loading: ");

      vm.initialize = initialize;
      vm.save = save;

      function initialize(blockId, serviceId) {
        vm.service = DataServices.getService(blockId, serviceId);
        vm.service.clearAlerts();
        DataServices.loadCopyToServices(blockId, vm.service);
        DataServices.loadAccounts(blockId, vm.service);
        DataServices.loadSubservices(blockId, vm.service);
      }  
      
      function save() {
        DataServices.save(vm.service);
      }
  }
})();