/*global angular*/
module.exports = (function() {
  'use strict';

  angular
    .module('serviceAccounts')
    .controller('SubserviceListController', SubserviceListController);

  SubserviceListController.$inject = ['$log', '_', 'DataServices', '$scope', '$interval', 'uiGridConstants'];


  function SubserviceListController($log, _, DataServices, $scope, $interval, uiGridConstants) {
    var vm = this;
    $log.debug("SubserviceListController: loading: ");

    vm.initialize = initialize;

    function initialize(blockId, serviceId) {
      $log.debug("SubserviceListController: initialize: blockId: " + blockId + " serviceId: " + serviceId);
      vm.service = DataServices.getService(blockId, serviceId);
      vm.blockId = blockId;
      activate();
    }

    function activate() {
      vm.service.clearAlerts();
      DataServices.loadSubservices(vm.blockId, vm.service).then(
        function(result) {
          if (angular.isDefined(vm.gridApi)) {
            vm.gridApi.selection.clearSelectedRows();
          }
          vm.gridOptions.data = result.getSubservices();

          $interval(function() {
            _.forEach(result.getSubservices(), function(subservice, index) {
              if (subservice.selected) {
                vm.gridApi.selection.selectRow(vm.gridOptions.data[index]);
              }
            });
          }, 0, 1);
        }
      );

    }

    vm.gridOptions = {
      enableSorting: false,
      enableColumnMenus: false,
      enableRowSelection: true,
      enableSelectAll: false,
      enableFullRowSelection: false,
      showGridFooter: true,
      enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER
    };

    vm.gridOptions.columnDefs = [{
      field: 'desc',
      name: 'Subservice',
      headerCellTemplate: '<div class="ui-grid-cell-contents">{{col.displayName}} for Account {{grid.appScope.service.selectedAccount}}</div>'
    }];

    vm.gridOptions.onRegisterApi = function(gridApi) {
      vm.gridApi = gridApi;
      gridApi.selection.on.rowSelectionChanged($scope,
        function(row) {
          $log.debug("SubserviceListController: subserviceGrid: rowSelected: " + row.isSelected);
          $log.debug("SubserviceListController: subserviceGrid: subserviceSelectedCount: " + vm.gridApi.selection.getSelectedRows().length);
          row.entity.selected = row.isSelected;
          if (vm.service.selectedAccount == "All") {
            vm.service.selectedSubservicesCount = vm.gridApi.selection.getSelectedRows().length;
          }
          else {
            var account = vm.service.getSelectedAccount();
            account.selectedSubservicesCount = vm.gridApi.selection.getSelectedRows().length;
          }
        }
      );
    }

    $scope.$on('subserviceListController::activate', function(event, service) {
      $log.debug("SubserviceListController: activate broadcast: " + event);
      activate();
    });
  }
})();