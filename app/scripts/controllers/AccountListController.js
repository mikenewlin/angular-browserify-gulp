/*global angular*/
module.exports = (function() {
  'use strict';

  angular
    .module('serviceAccounts')
    .controller('AccountListController', AccountListController);
      
  AccountListController.$inject = ['$log', '_', 'DataServices', '$scope', '$interval', 'uiGridConstants'];    

  function AccountListController($log, _, DataServices, $scope, $interval, uiGridConstants) {
    var vm = this;
    $log.debug("AccountListController: loading: ");
    
    vm.initialize = initialize;
    
    function initialize(blockId, serviceId) {
        $log.debug("AccountListController: initialize: blockId: " + blockId + " serviceId: " + serviceId);
        vm.service = DataServices.getService(blockId, serviceId);
        vm.service.clearAlerts();
        DataServices.loadAccounts(blockId, vm.service).then(
          function(result) {
            vm.gridOptions.data = result.getAccounts();
            $interval( function() {
              _.forEach(result.getAccounts(), function(account, index) {
                if (account.selected) {
                  vm.gridApi.selection.selectRow(vm.gridOptions.data[index]);
                }  
              });
            }, 0, 1);             
          }
        );
       
    }
    
    vm.gridOptions = {
      enableSorting: false,
      enableColumnMenus: false,
      enableRowSelection: true,
      enableSelectAll: false,
      enableFullRowSelection: false,
      showGridFooter:true,
      enableHorizontalScrollbar:  uiGridConstants.scrollbars.NEVER
    };
  
    vm.gridOptions.columnDefs = [
      { name: 'Account',
        cellTemplate:'<div class="ui-grid-cell-contents pull-left">{{row.entity.account}}</div><div class="ui-grid-cell-contents pull-right"><span tooltip-placement="bottom" tooltip="subservice required for account" ng-show="row.entity.hasWarning(grid.appScope.service.allSubServices)"><i class="fa fa-warning fa-fw fa-lg"></i></span><a ng-show="! grid.appScope.service.allSubServices && row.entity.selected" ng-click="row.entity.showSubserviceList(grid.appScope.service)"><i class="fa fa-pencil-square-o fa-fw fa-lg"></i></a></div>' 
      }
    ];  
  
    vm.gridOptions.onRegisterApi = function(gridApi) {
      vm.gridApi = gridApi;
      gridApi.selection.on.rowSelectionChanged($scope,
        function(row) {
          $log.debug("AccountListController: accountGrid: rowSelected: " + row.isSelected);
          $log.debug("AccountListController: accountGrid: accountSelectedCount: "  + vm.gridApi.selection.getSelectedRows().length);
          row.entity.selected = row.isSelected;
          vm.service.showSubservices = false;
          vm.service.selectedAccountCount = vm.gridApi.selection.getSelectedRows().length;
        }
      );
    }
 
  }
})();