/*global angular*/
module.exports = (function() {
    'use strict';

    angular
        .module('serviceAccounts')
        .controller('ServiceController', ServiceController);
        
    ServiceController.$inject = ['$log', 'DataServices'];  

    function ServiceController ($log, DataServices) {
        var vm = this;
        $log.debug("ServiceController: loading: ");

        vm.initialize = initialize;
        vm.accountTemplate = 'accounts.html';

        function initialize(id) {
            $log.debug("ServiceController: initialize: " + id);
            vm.block = DataServices.getBlock(id);
            activate();
        }
        
        function activate() {
            $log.debug("ServiceController: activate: " + vm.block.id);
            vm.block.clearAlerts();
            DataServices.getServiceList(vm.block.id)
                .then(function (result) {
                    vm.services = result;
                    $log.debug("ServiceController: getServiceList: load services");
                }, function (error) {
                    vm.block.addAlert({type: 'danger', msg: error.statusText});
                    $log.debug("ServiceController: getServiceList error: " + error.statusText);
                }
            ); 
            $log.debug("ServiceController: activate finished: ");
        }
    }
})();    
