module.exports = function() {

    return {
        restrict: 'E',
        scope: {
            blocks: '=' //Two-way data binding
        },
        template: '<ul><li ng-repeat="block in blocks">{{ block.desc }}</li></ul>'
    };
}