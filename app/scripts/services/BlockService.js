/*global angular*/
module.exports = (function() {
    'use strict';

    angular
        .module('serviceAccounts')
        .service('BlockService', BlockService);
      
    BlockService.$inject = ['$log', '$q', '_'];    

    function BlockService($log, $q, _) {
    	var blocks = [
    	    {"id": "1", "desc": "Block1"},
    	    {"id": "2", "desc": "Block2"},
    	    {"id": "3", "desc": "Block3"},
    	    {"id": "4", "desc": "Block4"},
    	    {"id": "5", "desc": "Block5"},
    	    {"id": "6", "desc": "Block6"},
    	    {"id": "7", "desc": "Block7"},
    	    {"id": "8", "desc": "Block8"},
    	];
    
    	this.getList = function() {
    	    var deferred = $q.defer();
    		deferred.resolve(blocks);
    		return deferred.promise;
    	};
    }
})();  