/*global angular*/
module.exports = (function() {
    'use strict';

    angular
        .module('serviceAccounts')
        .service('AccountService', AccountService);
      
    AccountService.$inject = ['$log', '$q', '_'];  
    
    function AccountService($log, $q, _) {
        var blockserviceaccounts = [
    	    {"id": "1", "serviceId": "1", "accounts": [
            	    {"account": "0022005561", "selected": true},
                    {"account": "0022003399", "selected": false},
                    {"account": "0033004433", "selected": false},
                    {"account": "0033449300", "selected": false},
            	    {"account": "0222034560", "selected": true}	        
    	        ]
    	    },
    	    {"id": "1", "serviceId": "2", "accounts": [
            	    {"account": "0022005561", "selected": false},
                    {"account": "0022003399", "selected": false},
                    {"account": "0033004433", "selected": true},
                    {"account": "0033449300", "selected": false},
            	    {"account": "0222034560", "selected": false}	        
    	        ]
    	    },
    	    {"id": "1", "serviceId": "3", "accounts": [
            	    {"account": "0022005561", "selected": false},
                    {"account": "0022003399", "selected": false},
                    {"account": "0033004433", "selected": false},
                    {"account": "0033449300", "selected": false},
            	    {"account": "0222034560", "selected": false}	        
    	        ]
    	    },	
    	    {"id": "1", "serviceId": "4", "accounts": [
            	    {"account": "0022005561", "selected": false},
                    {"account": "0022003399", "selected": false},
                    {"account": "0033004433", "selected": false},
                    {"account": "0033449300", "selected": false},
            	    {"account": "0222034560", "selected": false}	        
    	        ]	    },	
    	    {"id": "1", "serviceId": "5", "accounts": [
            	    {"account": "0022005561", "selected": false},
                    {"account": "0022003399", "selected": false},
                    {"account": "0033004433", "selected": false},
                    {"account": "0033449300", "selected": false},
            	    {"account": "0222034560", "selected": false}	        
    	        ]
    	    },	    
    	    {"id": "2", "serviceId": "6", "accounts": [
            	    {"account": "0022005561", "selected": false},
                    {"account": "0022003399", "selected": false},
                    {"account": "0033004433", "selected": false},
                    {"account": "0033449300", "selected": false},
            	    {"account": "0222034560", "selected": false}	        
    	        ]
    	    },	 
    	    {"id": "2", "serviceId": "7", "accounts": [
            	    {"account": "0022005561", "selected": false},
                    {"account": "0022003399", "selected": false},
                    {"account": "0033004433", "selected": false},
                    {"account": "0033449300", "selected": false},
            	    {"account": "0222034560", "selected": false}	        
    	        ]
    	    }	    
        ];
        
        this.getList = function(blockId, serviceId) {
            var deferred = $q.defer();
            var blockservice = _.find(blockserviceaccounts, {id: blockId, serviceId: serviceId});
            if (blockservice) {
                deferred.resolve(blockservice.accounts);    
            }
            else {
                deferred.reject({"statusText": "No Accounts found for service Id", "statusCd": "404"});    
            }
            return deferred.promise;
        };
    }    
})();    