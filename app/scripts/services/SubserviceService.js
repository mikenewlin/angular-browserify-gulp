/*global angular*/
module.exports = (function() {
    'use strict';

    angular
        .module('serviceAccounts')
        .service('SubserviceService', SubserviceService);
      
    SubserviceService.$inject = ['$log', '$q', '_'];  
    
    function SubserviceService($log, $q, _) {
	    var subservices = [
		    {"id": "1", "serviceId": "1", "account": "0", "subservices": [
		       {"id": "1", "desc": "subservice1", "selected": false},
		       {"id": "2", "desc": "subservice2", "selected": false},
		       {"id": "3", "desc": "subservice3", "selected": false},
		    ]},	         
		    {"id": "1", "serviceId": "1", "account": "0022005561", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},
		    {"id": "1", "serviceId": "1", "account": "0022003399", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	    
		    {"id": "1", "serviceId": "1", "account": "0033004433", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	    
		    {"id": "1", "serviceId": "1", "account": "0033449300", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	 
		    {"id": "1", "serviceId": "1", "account": "0222034560", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	 	
		    {"id": "1", "serviceId": "2", "account": "0", "subservices": [
		       {"id": "1", "desc": "subservice1", "selected": false},
		       {"id": "2", "desc": "subservice2", "selected": false},
		       {"id": "3", "desc": "subservice3", "selected": false},
		    ]},	         
		    {"id": "1", "serviceId": "2", "account": "0022005561", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},
		    {"id": "1", "serviceId": "2", "account": "0022003399", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	    
		    {"id": "1", "serviceId": "2", "account": "0033004433", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	    
		    {"id": "1", "serviceId": "2", "account": "0033449300", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	 
		    {"id": "1", "serviceId": "2", "account": "0222034560", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	 			    
		    {"id": "1", "serviceId": "3", "account": "0", "subservices": [
		       {"id": "1", "desc": "subservice1", "selected": false},
		       {"id": "2", "desc": "subservice2", "selected": false},
		       {"id": "3", "desc": "subservice3", "selected": false},
		    ]},	         
		    {"id": "1", "serviceId": "3", "account": "0022005561", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},
		    {"id": "1", "serviceId": "3", "account": "0022003399", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	    
		    {"id": "1", "serviceId": "3", "account": "0033004433", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	    
		    {"id": "1", "serviceId": "3", "account": "0033449300", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	 
		    {"id": "1", "serviceId": "3", "account": "0222034560", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	 			    
		    {"id": "1", "serviceId": "4", "account": "0", "subservices": [
		       {"id": "1", "desc": "subservice1", "selected": false},
		       {"id": "2", "desc": "subservice2", "selected": false},
		       {"id": "3", "desc": "subservice3", "selected": false},
		    ]},	         
		    {"id": "1", "serviceId": "4", "account": "0022005561", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},
		    {"id": "1", "serviceId": "4", "account": "0022003399", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	    
		    {"id": "1", "serviceId": "4", "account": "0033004433", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	    
		    {"id": "1", "serviceId": "4", "account": "0033449300", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	 
		    {"id": "1", "serviceId": "4", "account": "0222034560", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	 			    
		    {"id": "1", "serviceId": "5", "account": "0", "subservices": [
		       {"id": "1", "desc": "subservice1", "selected": false},
		       {"id": "2", "desc": "subservice2", "selected": false},
		       {"id": "3", "desc": "subservice3", "selected": false},
		    ]},	         
		    {"id": "1", "serviceId": "5", "account": "0022005561", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},
		    {"id": "1", "serviceId": "5", "account": "0022003399", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	    
		    {"id": "1", "serviceId": "5", "account": "0033004433", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	    
		    {"id": "1", "serviceId": "5", "account": "0033449300", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	 
		    {"id": "1", "serviceId": "5", "account": "0222034560", "subservices": [
		       {"id": "4", "desc": "accountsubservice4", "selected": false},
		       {"id": "5", "desc": "accountsubservice5", "selected": false},
		       {"id": "6", "desc": "accountsubservice6", "selected": false},
		    ]},	 			    
		    {"id": "2", "serviceId": "6", "account": "0", "subservices": [
		       {"id": "7", "desc": "subservice7", "selected": false},
		       {"id": "8", "desc": "subservice8", "selected": false},
		    ]},	         
		    {"id": "2", "serviceId": "6", "account": "0022005561", "subservices": [
		       {"id": "9", "desc": "accountsubservice9", "selected": false},
		       {"id": "10", "desc": "accountsubservice10", "selected": false},
		    ]},
		    {"id": "2", "serviceId": "6", "account": "0022003399", "subservices": [
		       {"id": "9", "desc": "accountsubservice9", "selected": false},
		       {"id": "10", "desc": "accountsubservice10", "selected": false},
		    ]},	    
		    {"id": "2", "serviceId": "6", "account": "0033004433", "subservices": [
		       {"id": "9", "desc": "accountsubservice9", "selected": false},
		       {"id": "10", "desc": "accountsubservice10", "selected": false},
		    ]},	    
		    {"id": "2", "serviceId": "6", "account": "0033449300", "subservices": [
		       {"id": "9", "desc": "accountsubservice9", "selected": false},
		       {"id": "10", "desc": "accountsubservice10", "selected": false},
		    ]},	 
		    {"id": "2", "serviceId": "6", "account": "0222034560", "subservices": [
		       {"id": "9", "desc": "accountsubservice9", "selected": false},
		       {"id": "10", "desc": "accountsubservice10", "selected": false},
		    ]},	 	 
		    {"id": "2", "serviceId": "8", "account": "0", "subservices": [
		       {"id": "11", "desc": "subservice11", "selected": false},
		       {"id": "12", "desc": "subservice12", "selected": false},
		    ]},	 	    
	    ];
	    
        this.getList = function(blockId, serviceId) {
            var deferred = $q.defer();
            var subservice = _.find(subservices, {id: blockId, serviceId: serviceId, account: "0"});
            if (subservice) {
                deferred.resolve(subservice.subservices);    
            }
            else {
                deferred.reject({"statusText": "No Subservices found for service Id", "statusCd": "404"});  
            }
            return deferred.promise;
        };

        this.getAccountList = function(blockId, serviceId, account) {
            var deferred = $q.defer();
            var subservice = _.find(subservices, {id: blockId, serviceId: serviceId, account: account});
            if (subservice) {
                deferred.resolve(subservice.subservices);    
            }
            else {
                deferred.reject({"statusText": "No Subservices found for service Id and account", "statusCd": "404"});  
            }
            return deferred.promise;
        };	    
    }
})();    