/*global angular*/
module.exports = (function() {
    'use strict';

    angular
        .module('serviceAccounts')
        .service('ServiceService', ServiceService);
      
    ServiceService.$inject = ['$log', '$q', '_'];  
    
    function ServiceService($log, $q, _) {
        var blockservices = [
    	    {"id": "1", "services": [
            	    {"id": "1", "desc": "block1 service1", "allAccounts": true, "allSubServices": true},
                    {"id": "2", "desc": "block1 service2", "allAccounts": false, "allSubServices": true},
                    {"id": "3", "desc": "block1 service3", "allAccounts": true, "allSubServices": false},
                    {"id": "4", "desc": "block1 service4", "allAccounts": false, "allSubServices": false},
            	    {"id": "5", "desc": "block1 service5", "allAccounts": true, "allSubServices": true}	        
    	        ]
    	    },
    	    {"id": "2", "services": [
            	    {"id": "6", "desc": "block2 service6", "allAccounts": true, "allSubServices": true},
                    {"id": "7", "desc": "block2 service7", "allAccounts": true, "allSubServices": false},
                    {"id": "8", "desc": "block2 service8", "allAccounts": true, "allSubServices": true},
                    {"id": "9", "desc": "block2 service9", "allAccounts": true, "allSubServices": true},
            	    {"id": "10", "desc": "block2 service10", "allAccounts": true, "allSubServices": true}	        
    	        ]
    	    },	    
    	    {"id": "3", "services": [
            	    {"id": "11", "desc": "block3 service11", "allAccounts": true, "allSubServices": true},
                    {"id": "12", "desc": "block3 service12", "allAccounts": true, "allSubServices": true}
    	        ]
    	    },
    	    {"id": "4", "services": [
            	    {"id": "13", "desc": "block4 service13", "allAccounts": true, "allSubServices": true},
                    {"id": "14", "desc": "block4 service14", "allAccounts": true, "allSubServices": true}
    	        ]
    	    },
    	    {"id": "5", "services": [
            	    {"id": "15", "desc": "block5 service15", "allAccounts": true, "allSubServices": true},
                    {"id": "16", "desc": "block5 service16", "allAccounts": true, "allSubServices": true}
    	        ]
    	    },	  
    	    {"id": "6", "services": [
            	    {"id": "17", "desc": "block6 service17", "allAccounts": true, "allSubServices": true},
    	        ]
    	    },	
    	    {"id": "7", "services": [
            	    {"id": "18", "desc": "block7 service18", "allAccounts": true, "allSubServices": true},
    	        ]
    	    }
        ];
        
        this.getList = function(blockId) {
            var deferred = $q.defer();
            var block = _.find(blockservices, {id: blockId});
            if (block) {
                deferred.resolve(block.services);    
            }
            else {
                deferred.reject({"statusText": "No Services found for Block Id", "statusCd": "404"});   
            }
            return deferred.promise;
        };
        
        this.save = function(model) {
            var json = JSON.stringify(model);
            $log.debug("saving model: " + json);
            var deferred = $q.defer();
            if (_.isEmpty(model.changes)) {
                deferred.reject({"statusText": "No changes to save", "statusCd": "503"});
            }
            else {
            //deferred.resolve(block.services);    
                deferred.reject({"statusText": "Save not implemented", "statusCd": "503"});   
            }    
            return deferred.promise;
        };        
    }    
})();        