/*global angular*/
module.exports = (function() {
    'use strict';
    
    angular
        .module('serviceAccounts')
        .config(Routes);
        
    Routes.$inject = ['$logProvider', '$stateProvider', '$urlRouterProvider'];  


    function Routes($logProvider, $stateProvider, $urlRouterProvider) {
        $logProvider.debugEnabled(true);
        $urlRouterProvider.otherwise('/blocks');
        
        $stateProvider
            
            // HOME STATES AND NESTED VIEWS ========================================
            .state('blocks', {
                url: '/blocks',
                templateUrl: 'blocks.html',
                controller: 'BlockController',
                controllerAs: 'blockVm'
    
            })
    
            // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
            .state('about', {
                // we'll get to this in a bit       
            });
    }    
        
})();